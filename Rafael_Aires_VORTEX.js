/*Explicando um pouco do código, percebi um padrão bem simples na partida,
o fato é que o número de possibilidades de jogadores livres sempre é = a quantidade de jogadores -1
até chegar em um valor fixo que vai se repetir até o tempo acabar, por exemplo 
Jogadores livres = 4+3+2+2+2... e o valor 2 se repete até o tempo acabar*/
//Código em JavaScript

function jogo(t,q,x){
    
    if(t > 0 && t <= 24 && q >= 2 && q <=5 && x > 0 && x <= q-1){
        let posfixa = q-x  //let posfixa = possibilidade fixa de passes
        let posvariavel = 0  //let posvariavel = possibilidade variável de passes
        let segundos = 0  //let segundos = segundos gastos 
        let armazena = 0  //let armazena = apenas usada para armazenar o valor das possibilidades fixas
        
        //calculo da possibilidade variável
        for(let i = q-1; i > posfixa;i--){
            posvariavel = posvariavel + i
            segundos++
        }
        
        //calculo da possibilidade fixa
        for(let i = t-segundos; i > 0;i--){
            armazena = armazena + posfixa
        }

        return armazena+posvariavel
    }else{
        return console.log('Valores Inválidos')
    }
}

console.log(jogo(3,3,2))
console.log(jogo(7,5,3))
console.log(jogo(10,2,1))
console.log(jogo(12,4,3))
console.log(jogo(24,3,2))